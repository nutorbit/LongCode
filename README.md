# longcode

> ITF final project.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

# Color
Clean -> #DFDCE3, #CCC, #737373
NAVY BLUE -> #18121E #4b4257
GUNMETAL -> #233237  #dce6e9
RUSTY RED -> #984B43
WARM -> #EAC67A
SALMON -> #D94B3F #d5382a



For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
